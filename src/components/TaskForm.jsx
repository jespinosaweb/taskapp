import { useState, useContext } from "React";
import { TaskContext } from "../context/TaskContext";

function TaskForm() {
  const [title, setTitle] = useState("");
  const [descripction, setDescripction] = useState("");
  const { createTask } = useContext(TaskContext);

  const handleSubmit = (e) => {
    e.preventDefault();
    createTask({ title, descripction });
    setTitle("");
    setDescripction("");
  };

  return (
    <div className="max-w-md mx-auto">
      <form onSubmit={handleSubmit} className="bg-slate-800 p-10 mb-4">
        <h1 className="text-2xl font-bold text-white mb-3">Crea tu tarea</h1>
        <input
          placeholder="Escribe tarea"
          onChange={(e) => setTitle(e.target.value)}
          value={title}
          autoFocus
          className="bg-slate-300 p-3 w-full mb-2"
        />
        <br />
        <textarea
          placeholder="Escribe una descripcion"
          onChange={(e) => setDescripction(e.target.value)}
          value={descripction}
          className="bg-slate-300 p-3 w-full mb-2"
        ></textarea>
        <br />
        <button className="bg-indigo-500 px-3 py-1 text-white hover:bg-indigo-400">
          Guardar
        </button>
      </form>
    </div>
  );
}

export default TaskForm;
